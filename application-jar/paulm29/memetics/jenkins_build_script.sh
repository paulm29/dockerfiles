#!/bin/bash

##############################
# Testing - set these using bash shell
##############################
# JAR_FILE=memetics-0.0.1-SNAPSHOT.jar
# DOCKER_DIR=https://raw.githubusercontent.com/paulm29/dockerfiles/master/application-jar
# DOCKER_IMAGE_NAME=paulm29/memetics
# DOCKER_IMAGE_NAME_TAG=memetics
##############################



##############################
# Jenkins shell script - variables pass through jenkins parameters
##############################

echo "Jar file: " $JAR_FILE
#cp "$JAR_FILE" .

VERSION_JAR=$(basename "$JAR_FILE")
ARGS="--build-arg VERSION_JAR=$VERSION_JAR"

echo "downloading Dockerfile..."
wget $DOCKER_DIR/$DOCKER_IMAGE_NAME/Dockerfile

echo "Building docker image..."
#docker build --build-arg VERSION_JAR=$(basename "$JAR_FILE") -t $DOCKER_IMAGE_NAME -f Dockerfile
docker build -t $DOCKER_IMAGE_NAME $ARGS .

echo "Logging into dockerhub..."
docker login -u paulm29 -p [PASSWORD]

# don't think this is necessary, just extra time and bandwidth?!
#echo "Pushing image to dockerhub..."
#docker push $DOCKER_IMAGE_NAME

echo "Tagging image..."
DOCKER_IMAGE_NAME_TAG=$DOCKER_IMAGE_NAME:latest
docker tag $DOCKER_IMAGE_NAME $DOCKER_IMAGE_NAME_TAG

echo "Pushing tag..."
docker push $DOCKER_IMAGE_NAME_TAG

##############################