#!/bin/bash

# e.g. ./startSate3App.sh paulm29/memetics 8989 ci

if [ "$#" -ne 3 ]; then
  echo "Usage: $(basename $0) service name port profile"
  exit 1
fi

NAME=$1
PORT=$2
PROFILE=$3

vexec() { echo "\$ $@" ; "$@" ; }

#REPOSITORY=https://hub.docker.com

APP_DIR=/var/opt/app_base/${NAME}
APP_LOG_DIR=${APP_DIR}/logs
mkdir -p $APP_LOG_DIR

DOCKER_NAME=$(basename "$NAME")
DOCKER_VOL="-v ${APP_DIR}:/var/opt/app_base/sate:rw -v /etc/localtime:/etc/localtime"
DOCKER_NET="-p ${PORT}:8080"
DOCKER_OPT="-d"
DOCKER_IMG="${NAME}:latest"
DOCKER_ENV="ENV_OPTS=-Dspring.profiles.active=${PROFILE}"
#DOCKER_LOG=--log-opt max-size=100m --log-opt max-files=5

#Stop the container if it is running
RUNNING=$(docker ps | grep ${DOCKER_NAME} | wc -l)
if (( ${RUNNING} > 0)); then
  echo Stopping docker container: $DOCKER_NAME
  docker stop $DOCKER_NAME
fi

#Remove the container's image
EXISTING=$(docker ps -a | grep ${DOCKER_NAME} | wc -l)
if (( ${EXISTING} > 0)); then
  echo Removing docker container image: $DOCKER_NAME
  docker rm $DOCKER_NAME
fi

#Get the latest image from the repository
docker pull $DOCKER_IMG

#$ docker run -d -p 8989:8080 -v /var/opt/app_base/paulm29/memetics:/var/opt/app_base/sate:rw
#    -v /etc/localtime:/etc/localtime
#    -e ENV_OPTS=-Dsate.log.path=/var/opt/app_base/paulm29/memetics/logs
#    -Dspring.profiles.active=ci
#    --name memetics paulm29/memetics:latest

vexec docker run $DOCKER_OPT $DOCKER_LOG $DOCKER_NET $DOCKER_VOL -e "$DOCKER_ENV" --name $DOCKER_NAME $DOCKER_IMG
